<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bid;
use Faker\Generator as Faker;

$factory->define(Bid::class, function (Faker $faker) {
    return [
        "amount" => $faker->randomNumber,
        "comment" => $faker->sentence,
        "user_id" => factory(App\User::class),
        "job_id" => factory(App\Job::class),
        
    ];
});
