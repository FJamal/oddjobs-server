<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Job;
use Faker\Generator as Faker;

$factory->define(Job::class, function (Faker $faker) {
    return [
        "title" => $faker->sentence,
        "description" => $faker->paragraph,
        "type" => $faker->randomElement(["plumber", "electrician", "carpenter", "painter"]),
        "user_id" => factory(App\User::class),
        "city" => $faker->randomElement(["Karachi", "Lahore", "Islamabad"]),
    ];
});
