<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::post("/register", function (Request $request) {
//   $request = $request->json()->all();
//   return $request;
// });

Route::post("/register", "ApiController@register");
Route::post("/login", "ApiController@login");
Route::post("/settings", "ApiController@settings");
// Route::post("/imagetest", function (Request $request) {
//   return ["uploaded" => $request->hasFile("photo")];
// });

Route::post("/saveProfile", "ApiController@saveProfile");

//for Posting of Jobs
Route::post("/postjob", "JobController@create");
//for searching job
Route::post("/searchjob", "JobController@show");
//for submission of bids
Route::post("/submitbid", "BidController@create");
//to get number for bids submitted for a job used by apps searched job detail screen
Route::post("/getbids", "JobController@getBidsCount");
//to get all the bids for a selected job
Route::post("/getallbids", "BidController@show");
//to save acceptence of bid on server
Route::post("/acceptbid", "BidController@accept");
//get the conversations for inbox
Route::post("/inbox", "ConversationController@show");
//to send messages
Route::post("/sendmessage", "ConversationController@savemessage");
//to make a conversation as seen in db
Route::post("/messageseen", "ConversationController@messageseen");
//Get all the messages for a chat to be displayed in messageWindow.js
Route::post("/getmessages", "ConversationController@getmessages");
// to remove the new message notification from server if there is any
Route::post("/removemessagenotification", "ConversationController@removeNewMessageNotification");
//to get all the bids of one user
Route::post("/getuserbids", "BidController@showUserBids");
//mark a job as compeleted
Route::post("/jobcompleted", "JobController@markJobCompleted");
//save ratings of successful bidders
Route::post("/saveratings", "JobController@saveratings"); 
