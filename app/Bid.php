<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = ["amount", "comment", "user_id", "job_id", "accepted"];

    public function user(){
      return $this->belongsTo(User::class);
    }

    public function job(){
      return $this->belongsTo(Job::class);
    }
}
