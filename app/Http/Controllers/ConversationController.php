<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Conversation;
use App\Message;
use App\MessageNotification;

class ConversationController extends Controller
{
    public function show(Request $request) {
      $data = $request->json()->all();
      //get the users details
      $user = User::where("api_token", $data["api_token"])->first();
      //get all the conversations for this user
      $conversations = Conversation::where("user_one", $user->id)
                      ->orWhere("user_two", $user->id)->get()->all();

      /*get the user detail of the other user like profile pic and etc to display
      in inbox*/
      foreach($conversations as $conversation)
      {
        if($conversation->user_one == $user->id)
        {
          $otherUserId = $conversation->user_two;
          //get user name and display pic
          $userInfo = $this->getUserData($otherUserId);

        }
        else if($conversation->user_two == $user->id)
        {
          $otherUserId = $conversation->user_one;
          //get user name and display pic
          $userInfo = $this->getUserData($otherUserId);
        }
        //adding that user info
        $conversation["userInfo"] = $userInfo;
        //adding all the messages for this conversation
        $conversation->messages = $conversation->messages;
      }

      return ["conversations" => $conversations];
    }




    public function getUserData($id) {
      $user = User::where("id", $id)->first();
      return ["name" => $user->name,
              "last_name" => $user->last_name,
              "profilePic" => $user->profilePic,
              "user_id" => $id];
    }



    /*Method to deal with functionallity when messages from chat are send*/
    public function savemessage(Request $request)
    {
      $data = $request->json()->all();
      //get this user data
      $user = User::where("api_token", $data["api_token"])->first();
      //check if there is already a conversation going on between the two users
      $conversation = Conversation::where([["user_one", $user->id], ["user_two", $data["userIdOfChat"]]])
                      ->orWhere([["user_one", $data["userIdOfChat"]], ["user_two",$user->id ]])->first();

      if($conversation)
      {
        //there alreay a conversation created between the two users
        Message::create(["conversation_id" => $conversation->id,
                          "user_id" => $user->id,
                          "message" => $data["message"]]);

        //update the conversation model to tell the new message is for which of the two users
        $conversation->update(["not_seen" => true,
                              "new_message_for" => $data["userIdOfChat"]]);

        /*Test*/
        $otherUser = User::where("id", $data["userIdOfChat"])->first();
        $otherUser->update(["new_message_notification" => true]);
        /*EndTest*/

        return ["success" => true];
      }
      else
      {
        //create a conversation plus a new message for that conversation
        $conversation = Conversation::create(["user_one" => $user->id,
                                              "user_two" => $data["userIdOfChat"],
                                              "new_messages" => true,
                                              "not_seen" => true,
                                              "new_message_for" => $data["userIdOfChat"]]);

        //then create a message for this conversation
        Message::create(["conversation_id" => $conversation->id,
                          "user_id" => $user->id,
                          "message" => $data["message"]]);

        /*Test*/
        $otherUser = User::where("id", $data["userIdOfChat"])->first();
        $otherUser->update(["new_message_notification" => true]);
        /*EndTest*/

        return["success" => true];
      }


    }


    //function that marks the conversation as seen in db
    public function messageseen(Request $request)
    {
      $data = $request->json()->all();
      $conversation = Conversation::where("id", $data["conversation_id"])->first();
      //update it
      $conversation->update(["not_seen" => false]);
      return["success" => true];
    }


    //To get all the messages for a particular conversation
    public function getmessages(Request $request)
    {
      $data = $request->json()->all();
      //get this user data
      $user = User::where("api_token", $data["api_token"])->first();
      //check if there is already a conversation going on between the two users
      $conversation = Conversation::where([["user_one", $user->id], ["user_two", $data["userIdOfChat"]]])
                      ->orWhere([["user_one", $data["userIdOfChat"]], ["user_two",$user->id ]])->first();
      //get the profile pic of the other user
      $otherUser = User::where("id", $data["userIdOfChat"])->first();
      //if condition to fix bug that if there was no conversation bw the two users
      if($conversation)
      {
        return["conversation" => $conversation->messages,
                "profilePic" => $otherUser->profilePic];

      }
      else
      {
        return["conversation" => null,
                "profilePic" => $otherUser->profilePic];
      }

    }



    public function removeNewMessageNotification(Request $request)
    {
      $data = $request->json()->all();
      $user = User::where("id", $data["user_id"])->first();
      $user->update(["new_message_notification" => false]);
      return ["newMessageNotification" => "removed"];
    }
}
