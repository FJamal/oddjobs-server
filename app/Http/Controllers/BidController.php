<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Job;
use App\Bid;
use App\Rating;

class BidController extends Controller
{
    public function create(Request $request)
    {
      $data = $request->json()->all();
      $validator = Validator::make($data , [
        "amount" => ["required", "numeric"],
        "comment" => ["max:250"]
      ]);

      if($validator->fails())
      {
        $error = $validator->errors()->first();
        return["error" => true, "errorMsg" => $error];
      }
      else
      {
        //the user
        $user = User::where("api_token", $data["api_token"])->first();
        //check if a bid for this job is already there
        $bid = Bid::where([
          "user_id" => $user->id,
          "job_id" => $data["job_id"],
        ])->first();

        if(!$bid)
        {
          Bid::create([
            "job_id" => $data["job_id"],
            "user_id" => $user->id,
            "amount" => $data["amount"],
            "comment" => $data["comment"]
          ]);
        }
        else
        {
          Bid::where([
            "job_id" => $data["job_id"],
            "user_id" => $user->id,])
          ->update([
            "amount" => $data["amount"],
            "comment" => $data["comment"],
            "accepted" => false,
          ]);
        }

        return["success" => true];
      }
    }


    public function show(Request $request)
    {
      //show all bids for one job
      $data = $request->json()->all();
      $bids = Bid::where("job_id", $data["job_id"])->get()->all();
      foreach($bids as $bid)
      {
        //get user info for that bidder
        $user = User::where("id", $bid->user_id)->first();
        $bid["name"] = $user->name;
        $bid["lastName"] = $user->last_name;
        $bid["profilePic"] = $user->profilePic;
        //adding his rating
        $rating = $user->rating;
        //if a rating for this user is already created in db
        if($rating)
        {
          $bid["rating"] = $user->rating->getRatings();
        }
        else
        {
          $bid["rating"] = 1;
        }

        //$bid["rating"] = $user->rating->getRatings();
      }
      return["bids" => $bids];
    }

    //to save bid status on server
    public function accept(Request $request)
    {
      $data = $request->json()->all();
      //first get all the bids for this job
      $bids = Bid::where("job_id", $data["job_id"])->get()->all();
      foreach ($bids as $bid)
      {
        //change every bid to false ie not  accepted
        $bid->update(["accepted" => false]);
      }
      //change one bids status to either true or false based on that bids id
      Bid::where("id", $data["bidId"])->update(["accepted" => $data["accepted"]]);
      return["accepted" => true];
    }

    public function showUserBids(Request $request)
    {
      $data = $request->json()->all();
      $user = User::where("api_token", $data["api_token"])->first();
      $bids = $user->bids;
      $data = [];
      foreach($bids as $bid)
      {
        //get the job data for this bid
        $job = $bid->job;
        // get the user info for job creator
        $user = $job->user;
        //adding all to data
        $data []= [ "jobId" => $job->id,
                    "jobTitle" => $job->title,
                    "jobDescription" => $job->description,
                    "jobImage1" => $job->image1,
                    "jobImage2" => $job->image2,
                    "jobType" => $job->type,
                    "bidAmount" => $bid->amount,
                    "bidAccepted" => $bid->accepted,
                    "userName" => $user->name,
                    "userProfilePic" => $user->profilePic,
                    "userId" => $user->id,
                    "jobCompleted" => $job->completed,
                    ];
      }

      return ["data" => $data];



    }
}
