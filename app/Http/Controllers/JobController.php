<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Job;
use App\Rating;

class JobController extends Controller
{
    public function create(Request $request)
    {
      //check if the job was posted with images or not
      if($request->has("photo1") || $request->has("photo0"))
      {
        /*logic for creating a job that was submitted with image/s*/
        //get the data
        $postData = json_decode($request->input("data"), true);
        $validate = $this->validation($postData);
        if($validate["error"] == true)
        {
          return $validate;
        }
        else
        {
          //Post the job in db as well as in cloudinary
          $user = User::where("api_token", $postData["api_token"])->first();
          //create a job first without the images
          $job = Job::create([
            "title" => $postData["title"],
            "description" => $postData["description"],
            "type" => $postData["type"],
            "user_id" => $user->id,
            "city" => $user->city,
          ]);

          $imagePaths = [];
          //if photo0 was submitted
          if($request->has("photo0"))
          {
            try
            {
              $result = $request->file("photo0")->storeOnCloudinaryAs("oddjobs/{$user->id}/{$job->id}", "photo0");
              //images path from cloudinary
              $path = $result->getPath();
              //adding to image array
              array_push($imagePaths, $path);
            } catch (Exception $e)
            {
              return ["error" => true, "errormsg" => $e->getMessage()];
            }

          }

          //if photo1 was submitted
          if($request->has("photo1"))
          {
            try
            {
              $result = $request->file("photo1")->storeOnCloudinaryAs("oddjobs/{$user->id}/{$job->id}", "photo1");
              //images path from cloudinary
              $path = $result->getPath();
              //adding to image array
              array_push($imagePaths, $path);
            } catch (Exception $e)
            {
              return ["error" => true, "errormsg" => $e->getMessage()];
            }

          }

          //now add the image links into the job model in db
          for($i = 1; $i < 3; $i++)
          {
            if($imagePaths[$i - 1])
            {
              $job->update([
                "image{$i}" => $imagePaths[$i - 1],

              ]);  
            }


          }

          return ["success" => "job created"];

          //sotre the dp image on cloudinary in a  folder with a name of oddjobs/usersid/jobid/name
          //$result = $request->file("photo0")->storeOnCloudinaryAs("oddjobs/{$user->id}/{$job->id}", "photo0");
          //images path from cloudinary
          //$image1Path = $result->getPath();
          //Code to delete an saved image
          //$result = cloudinary()->destroy('oddjobs/1/79/');

        }
      }
      else
      {
        //logic for creating a job that was submitted without images
        $data = $request->json()->all();
        $validate = $this->validation($data);
        if($validate["error"] == true)
        {
          return $validate;
        }
        else
        {

          $user = User::where("api_token", $data["api_token"])->first();
          $job = Job::create([
            "title" => $data["title"],
            "description" => $data["description"],
            "type" => $data["type"],
            "user_id" => $user->id,
            "city" => $user->city,
          ]);

          return ["success" => "job created"];
        }
      }
    }


    //Validation for job creation input
    public function validation($data)
    {
      $validator = Validator::make($data, [
        "title" => ["required", "max:255"],
        "description" => ["required"],
        "type" => ["required"]
      ]);

      if($validator->fails())
      {
        $errors = $validator->errors()->first();
        return ["error" => true, "errormsg" => $errors];
      }

      return ["error" => false];
    }


    public function show(Request $request)
    {
      $filterOptions = $request->json()->all();
      if($filterOptions["city"] && !$filterOptions["jobtype"])
      {
        //only city was selected
        //$jobs = Job::where("city", $filterOptions["city"])->get()->all();
        $jobs = Job::where([["city", $filterOptions["city"]],
                            ["completed", "=", false],
                            ["user_id", "!=", $filterOptions["userId"]],
                          ])->get()->all();

        return["jobs" => array_reverse($jobs)];
      }
      else if($filterOptions["city"] && $filterOptions["jobtype"])
      {
        //if city is also submitted
        $jobs = Job::where([
          ["type", $filterOptions["jobtype"]],
          ["city", $filterOptions["city"]],
          ["completed" , "=", false],
          ["user_id", "!=", $filterOptions["userId"]],
          ])->get()->all();

        return ["jobs" => array_reverse($jobs)];
      }
      else
      {
        /*display jobs from all cities of this particular type
        that are not completed and does not belong to this user*/
        $jobs = Job::where([["type", $filterOptions["jobtype"]],
                            ["completed",false],
                            ["user_id", "!=", $filterOptions["userId"]],
                            ])->get()->all();
        return ["jobs" => array_reverse($jobs)];
      }

    }

    public function getBidsCount(Request $request)
    {
      $data = $request->json()->all();
      $job = Job::where("id", $data["id"])->first();
      return ["bids" => count($job->bids)];
    }

    /*used by postedjobdetail component on client,
    aspects a job id
    returns the user info of user whose bid got accepted for that job if was job completed */
    public function markJobCompleted(Request $request)
    {
      $data = $request->json()->all();
      $job = Job::Where("id", $data["jobId"])->first();
      $job->update(["completed" => $data["value"]]);
      //if the job was marked as completed then return the user info of bidder
      if($data["value"])
      {
        $user = $job->getUserInfoOfAcceptedBid();
        return ["jobCompleted" => true,
                "userName" => $user->name . " " . $user->last_name,
                "user_id" => $user->id,
                "profilePic" => $user->profilePic,
                "city" => $user->city];
      }
      else
      {
        return ["jobCompleted" => false];
      }


    }

    public function saveratings(Request $request)
    {
      $data = $request->json()->all();
      $user = User::where("id", $data["userId"])->first();
      $rating = $user->rating;
      if($rating)
      {
        $rating->saveRatings($data["score"]);
        return ["ratingsSaved" => true];
      }
      else
      {
        //first create a rating for this user
        $rating = Rating::create(["user_id" => $data["userId"]]);
        $rating->saveRatings($data["score"]);
        return ["ratingsSaved" => true];
      }
    }
}
