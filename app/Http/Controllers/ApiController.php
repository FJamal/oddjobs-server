<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
  public function test(Request $request)
  {
    return $request->json()->all();
  }


    public function register(Request $request)
    {
      $request = $request->json()->all();

      $validator = Validator::make($request, [
        "name" => ["required", "string", "max:255"],
        "last_name" => ["required", "string", "max:255"],
        "email" => ["required", "string", "email:filter", "unique:App\User,email"],
        "password" => ["required", "string", "min:8", "confirmed"],
      ]);

      if($validator->fails())
      {
        $errors = $validator->errors()->first();
        return ["error" => $errors];
      }

      //storing in DB
      $user = User::create([
        "name" => $request["name"],
        "last_name" => $request["last_name"],
        "email" => $request["email"],
        "password" => \Hash::make($request["password"]),
        "api_token" => \Str::random(80),
      ]);

      return ["api_token" => $user->api_token];
    }


    public function login(Request $request)
    {
      $request = $request->json()->all();

      $validator = Validator::make($request, [
        "email" => ["required", "email:filter"],
        "password" => ["required"]
      ]);

      if($validator->fails())
      {
        $error = $validator->errors()->first();
        return ["error" => $error];
      }

      //getting the user
      $user = User::where("email", $request["email"])->first();
      if(!$user)
      {
        return ["error" => "Invalid email id or password"];
      }
      //check for password provided
      else if(\Hash::check($request["password"], $user->password))
      {
        //send in the api_token as a successful loogin
        return ["apiToken" => $user->api_token];
      }
      else
      {
        //incorrect password
        return ["error" => "Invalid email id or password"];
      }



    }


    public function settings(Request $request)
    {
      $input = $request->json()->all();
      $user = User::where("api_token", $input["apiToken"])->first();
      // getting all the jobs of this user
      $jobs = $user->jobs;
      //reversing the jobs array so that in client latest job comes to top
      //$jobs = array_reverse($jobs);
      //storing the profile data
      $profile = ["name" => $user->name,
                "lastName" => $user->last_name,
                "address" => $user->address,
                "profilePic" => $user->profilePic,
                "city" => $user->city,
                "id" => $user->id,
                "MessageNotification" => $user->new_message_notification,];


      return ["profile" => $profile,
              "jobs" => $jobs];
    }

    public function saveProfile(Request $request)
    {

      //$profileData = json_decode($request->input("data"), true);
      //return ["reply" => $test];
      // $result = $request->file("photo")->storeOnCloudinaryAs('test', '1stImage');
      // return ["path" => $result->getPath()];
      //if input has pic attached
      if($request->has("photo"))
      {
        $profileData = json_decode($request->input("data"), true);
        $validator = Validator::make($profileData, [
          "name" => ["required", "string", "max:255"],
          "lastName" => ["required", "string", "max:255"],
          "address" => ["required", "string", "max:300"],
          "city" => ["required", "string"],
        ]);

        if($validator->fails())
        {
          $errors = $validator->errors()->first();
          return ["error" => $errors];
        }
        //get the user
        $user = User::where("api_token" , $profileData["api_token"])->first();
        //sotre the dp image on cloudinary in a oddjobs folder with a name of users id
        $result = $request->file("photo")->storeOnCloudinaryAs("oddjobs/users", $user->id);
        //images path from cloudinary
        $imagePath = $result->getPath();
        //store the profile in db
        $user->update([
          "name" => $profileData["name"],
          "last_name" => $profileData["lastName"],
          "address" => $profileData["address"],
          "city" => $profileData["city"],
          "profilePic" => $imagePath,
        ]);
        //return with success and new profile pic link
        return["success" => true,
                "profileLink" => $imagePath];
      }
      else
      {
        //FOR WHEN PROFILE UPDATED WITH AN OLD PIC
        $data = $request->json()->all();
        //validate the submitted profile
        $validator = Validator::make($data, [
          "name" => ["required", "string", "max:255"],
          "lastName" => ["required", "string", "max:255"],
          "address" => ["required", "string", "max:300"],
          "city" => ["required", "string"],
        ]);

        if($validator->fails())
        {
          $errors = $validator->errors()->first();
          return ["error" => $errors];
        }
        //get the user
        $user = User::where("api_token" , $data["api_token"])->first();
        //store the profile in db
        $user->update([
          "name" => $data["name"],
          "last_name" => $data["lastName"],
          "address" => $data["address"],
          "city" => $data["city"],
        ]);

        return ["success" => true];
      }
    }
}
