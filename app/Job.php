<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
      "title",
      "description",
       "type",
      "image1",
      "image2",
      "user_id",
      "completed",
      "city",
    ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }


  //to get this job's bids
  public function bids() {
    return $this->hasMany(Bid::class);
  }

  //returns the user info for the accpeted bid for this job
  public function getUserInfoOfAcceptedBid()
  {
    $bids = $this->bids;
    foreach($bids as $bid)
    {
      if($bid->accepted)
      {
        return $bid->user;
      }
    }
  }
}
