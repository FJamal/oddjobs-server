<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ["user_id", "score", "totalVotes"];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    
    public function saveRatings($score)
    {
      // $this->score += $score;
      // $this->totalVotes++;
      // $this->save();
      $this->update(["score" => $this->score + $score,
                      "totalVotes" => $this->totalVotes + 1]);
      return $this;
    }

    public function getRatings()
    {
      return round($this->score/$this->totalVotes, 1);
    }
}
