<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
  protected $fillable = ["user_one", "user_two", "new_messages", "not_seen", "new_message_for"];

  public function messages(){
    return $this->hasMany(Message::class);
  }
}
